  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary bg-info elevation-3">
      <!-- Brand Logo -->
      <a href="{{ route('backend.dashboard') }}" class="brand-link">
          @if (!empty($about))
              <img src="{{ asset($about->logo) }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                  style="width: 40px; height:50px" style="opacity: .8">
          @else
              <img src="{{ asset('logo/noimage.jpg') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                  style="width: 50px; height:50px" style="opacity: .8">
          @endif
          <span class="brand-text font-weight-light text-md">
              @if (!empty($about))
                  {{ $about->name_la }}
              @endif
          </span>
      </a>
      <!-- Sidebar -->
      <div class="sidebar">
          <!-- Sidebar Menu -->
          <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                  data-accordion="false">
                  <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
                  <li class="nav-item menu-open">
                      @foreach ($function_available as $item1)
                          @if ($item1->function->name == 'action_1')
                              <a href="{{ route('backend.dashboard') }}"
                                  class="nav-link text-white {{ Request::is('dashboard') ? 'active' : '' }}">
                                  <i class="nav-icon fas fa-tachometer-alt"></i>
                                  <p>
                                      ຫນ້າຫຼັກ
                                  </p>
                              </a>
                          @endif
                      @endforeach
                  </li>
                  <li class="dropdown-divider"></li>
                  {{-- ===================== ຈັດການຂໍ້ມູນຫຼັກ ======================= --}}
                  {{-- <li class="nav-item {{ (
                    strpos(Route::currentRouteName(), 'backend.employee') == 'backend.employee'
                    || strpos(Route::currentRouteName(), 'backend.customer') == 'backend.customer'
                    || strpos(Route::currentRouteName(), 'backend.customer_type') == 'backend.customer_type'
                    || strpos(Route::currentRouteName(), 'backend.land_type') == 'backend.land_type'
                    || strpos(Route::currentRouteName(), 'backend.supplier') == 'backend.supplier'
                    || strpos(Route::currentRouteName(), 'backend.item') == 'backend.item'
                    ) ? 'menu-open' : '' }}"> --}}
                  <li class="nav-item">
                    @foreach ($function_available as $item1)
                    @if ($item1->function->name == 'action_2')
                      <a href="#" class="nav-link text-white">
                          <i class="nav-icon fas fa-map-marked-alt"></i>
                          <p>
                              ຂໍ້ມູນທີ່ຢູ່
                              <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      @endif
                      @endforeach
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.village') }}" class="nav-link text-white">
                                  <i class="fas fa-angle-double-right nav-icon"></i>
                                  <p>ບ້ານ</p>
                              </a>
                          </li>
                      </ul>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.district') }}" class="nav-link text-white">
                                  <i class="fas fa-angle-double-right nav-icon"></i>
                                  <p>ເມືອງ</p>
                              </a>
                          </li>
                      </ul>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.province') }}" class="nav-link text-white">
                                  <i class="fas fa-angle-double-right nav-icon"></i>
                                  <p>ແຂວງ</p>
                              </a>
                          </li>
                      </ul>
                  </li>
                  {{-- <li class="nav-item">
                    <a href="{{ route('backend.IncomeExpendContent') }}" class="nav-link text-white">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            ບັນທຶກປະຈຳວັນ
                        </p>
                    </a>
                </li> --}}
                  <li class="dropdown-divider"></li>
                  <li class="nav-item">
                    @foreach ($function_available as $item1)
                    @if ($item1->function->name == 'action_3')
                      <a href="#" class="nav-link text-white">
                          <i class="nav-icon fas fa-database"></i>
                          <p>
                              ຈັດການຂໍ້ມູນ
                              <i class="fas fa-angle-left right"></i>
                              {{-- <span class="badge badge-info right">6</span> --}}
                          </p>
                      </a>
                      @endif
                      @endforeach
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.user') }}" class="nav-link text-white">
                                  <i class="fas fa-angle-double-right nav-icon"></i>
                                  <p>ພະນັກງານ</p>
                              </a>
                          </li>
                      </ul>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.role') }}" class="nav-link text-white">
                                  <i class="fas fa-angle-double-right nav-icon"></i>
                                  <p>ສິດນຳໃຊ້</p>
                              </a>
                          </li>
                      </ul>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.sector') }}" class="nav-link text-white">
                                  <i class="fas fa-angle-double-right nav-icon"></i>
                                  <p>ຂະແໜງການ</p>
                              </a>
                          </li>
                      </ul>
                      <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('backend.position') }}" class="nav-link text-white">
                                <i class="fas fa-angle-double-right nav-icon"></i>
                                <p>ຕຳແໜ່ງ</p>
                            </a>
                        </li>
                    </ul>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.salary') }}" class="nav-link text-white">
                                  <i class="fas fa-angle-double-right nav-icon"></i>
                                  <p>ຂັ້ນເງິນເດືອນ</p>
                              </a>
                          </li>
                      </ul>
                      {{-- <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="" class="nav-link text-white {{ Request::is('customers') ? 'active' : '' }}">
                                <i class="fas fa-angle-double-right nav-icon"></i>
                                <p>ລູກຄ້າ</p>
                            </a>
                        </li>
                    </ul> --}}
                  </li>
                  <li class="dropdown-divider"></li>
                  <li class="nav-item">
                    @foreach ($function_available as $item1)
                    @if ($item1->function->name == 'action_4')
                      <a href="{{ route('backend.PaySalary') }}" class="nav-link text-white">
                          <i class="nav-icon fas fa-money-bill-alt"></i>
                          <p>
                              ເບີກຈ່າຍເງິນເດືອນ
                          </p>
                      </a>
                      @endif
                      @endforeach
                  </li>
                  <li class="dropdown-divider"></li>
                  <li class="nav-item">
                    @foreach ($function_available as $item1)
                    @if ($item1->function->name == 'action_5')
                      <a href="#" class="nav-link text-white">
                          <i class="nav-icon fas fa-chart-line"></i>
                          <p>
                              ລາຍງານ
                              <i class="fas fa-angle-left right"></i>
                              {{-- <span class="badge badge-info right">6</span> --}}
                          </p>
                      </a>
                      @endif
                      @endforeach
                      {{-- <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.ProductsReport') }}" class="nav-link text-white">
                                  <i class="fas fa-angle-double-right nav-icon"></i>
                                  <p>ລາຍງານຂໍ້ມູນສິນຄ້າ</p>
                              </a>
                          </li>
                      </ul>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.OrdersReport') }}" class="nav-link text-white">
                                  <i class="fas fa-angle-double-right nav-icon"></i>
                                  <p>ລາຍງານການຊື້ນຳຜູ້ສະຫນອງ</p>
                              </a>
                          </li>
                      </ul> --}}
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.ReportUser') }}" class="nav-link text-white">
                                  <i class="fas fa-angle-double-right nav-icon"></i>
                                  <p>ລາຍງານຂໍ້ມູນພະນັກງານ</p>
                              </a>
                          </li>
                      </ul>
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.ReportPaySalary') }}" class="nav-link text-white">
                                  <i class="fas fa-angle-double-right nav-icon"></i>
                                  <p>ລາຍງານເບີກຈ່າຍເງິນເດືອນ</p>
                              </a>
                          </li>
                      </ul>
                  </li>
                  <li class="dropdown-divider"></li>
                  <li class="nav-item">
                    @foreach ($function_available as $item1)
                    @if ($item1->function->name == 'action_6')
                      <a href="#" class="nav-link text-white">
                          <i class="nav-icon fas fa-hospital-alt"></i>
                          <p>
                              ຂໍ້ມູນຫ້ອງການ
                              <i class="fas fa-angle-left right"></i>
                              {{-- <span class="badge badge-info right">6</span> --}}
                          </p>
                      </a>
                      @endif
                      @endforeach
                      {{-- <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.slide') }}" class="nav-link text-white">
                                  <i class="fas fa-angle-double-right nav-icon"></i>
                                  <p>ສະໄລຮູບພາບ</p>
                              </a>
                          </li>
                      </ul> --}}
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ route('backend.about') }}" class="nav-link text-white">
                                  <i class="fas fa-angle-double-right nav-icon"></i>
                                  <p>ຂໍ້ມູນຫ້ອງການ</p>
                              </a>
                          </li>
                      </ul>
                  </li>
                  <li class="dropdown-divider"></li>
                  {{-- ===================== ກ່ຽວກັບບໍລິສັດ ======================= --}}
                  {{-- <li class="nav-item">
                    <a href="{{ route('about-company') }}" class="nav-link {{ Request::is('about-companys') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-address-card"></i>
                        <p>
                            ກ່ຽວກັບບໍລິສັດ
                        </p>
                    </a>
                </li>
                <li class="dropdown-divider"></li> --}}
              </ul>
          </nav>
          <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
  </aside>
